'use strict';

// media queries
var viewportWidth = getScreenSize();
var mediaBreakpoint = {
	sm: 576,
	md: 768,
	lg: 992,
	xl: 1200
};(function () {
	var pathElem = document.querySelector(".preloader__border");
	var SVGRoadInstance = new SVGRoad(pathElem);

	SVGRoadInstance.setStrokeDasharrayInPercent(100);
	SVGRoadInstance.setStrokeDashoffsetInPercent(-6000);
})();

$(function () {

	$('.wrap-site').css({
		'display': 'flex'
	});
	// preloader
	$('html').removeClass('not-loaded');
	setTimeout(function () {
		AOS.init();
	}, 600);

	objectFitFallback($("[data-object-fit]"));

	{
		var header = document.querySelector("header");
		var headroom = new Headroom(header, {
			offset: header.offsetTop
		});
		headroom.init();
	}

	// mask for input tel
	$(".js-tel").mask("+7 (999) 999-99-99", { placeholder: "+7 (___) ___-__-__" });

	$('.top-block__slider').on('init reInit afterChange', function (event, slick, currentSlide) {
		var index = currentSlide ? currentSlide : 0;
		$('.top-block__btn').each(function (i, item) {
			if (index == i) {
				item.classList.remove('d-none');
			} else {
				item.classList.add('d-none');
			}
		});
	}).on('lazyLoaded', function (event, slick, image, imageSource) {
		objectFitFallback($("[data-object-fit]", slick.$slider));
	}).slick({
		infinite: true,
		touchThreshold: 10,
		lazyLoad: 'progressive',
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: true,
		dots: true,
		cssEase: 'ease-in-out'
	});

	$('.partners__slider').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: false,
		swipeToSlide: true,
		touchThreshold: 10,
		dots: false,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnHover: false,
		pauseOnFocus: false,
		responsive: [{
			breakpoint: mediaBreakpoint.md,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: mediaBreakpoint.lg,
			settings: {
				slidesToShow: 3
			}
		}]
	});

	$('.staff-slider').slick({
		infinite: true,
		slidesToShow: 3,
		swipeToSlide: true,
		touchThreshold: 10,
		centerMode: true,
		centerPadding: 0,
		focusOnSelect: true,
		arrows: true,
		dots: false,
		responsive: [{
			breakpoint: mediaBreakpoint.md,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: mediaBreakpoint.lg,
			settings: {
				slidesToShow: 1,
				centerPadding: '160px'
			}
		}]
	});

	$('.hall__slider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
		var i = (currentSlide ? currentSlide : 0) + 1;
		slick.$slider.next().find('.slick-paging__current').text(i).end().find('.slick-paging__all').text(slick.slideCount);
	}).slick({
		infinite: true,
		touchThreshold: 10,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: true,
		dots: true,
		cssEase: 'ease-in-out',
		responsive: [{
			breakpoint: mediaBreakpoint.xl,
			settings: {
				adaptiveHeight: true
			}
		}]
	});

	{
		$('.equipment__slider').slick({
			infinite: true,
			slidesToShow: 1,
			fade: true,
			dots: false
		});

		$(document).on('click', '.equipment__item-link', function (e) {
			e.preventDefault();
			var $t = $(this);
			var id = $t.attr('data-id');

			if (!$t.parent().hasClass('active')) {
				$('.equipment__item').removeClass('active');
				$t.parent().addClass('active');

				$('.equipment__text').fadeOut(0);
				$t.closest('.equipment').find('[data-content-id=' + id + ']').fadeIn(0);
			} else {
				$t.parent().removeClass('active');
				$('.equipment__text').fadeOut(200);
			}
		});
	}

	$('.js-grid').masonry({
		columnWidth: '.grid-sizer',
		itemSelector: '.grid-item',
		percentPosition: true
	});

	// modals
	$.arcticmodal('setDefault', {
		overlay: {
			css: {
				backgroundColor: '#EFEFEF',
				opacity: 0.85
			}
		},
		openEffect: {
			speed: 200
		},
		closeEffect: {
			speed: 200
		},
		beforeOpen: function beforeOpen(data, el) {
			$('.header').css('paddingRight', getScrollbarSize());
		},
		afterClose: function afterClose(data, el) {
			$('.header').removeAttr('style');
		}
	});
	$(document).on('click', '.ajax-popup', function (e) {
		e.preventDefault();
		$.arcticmodal('close');
		var url = $(this).attr('href');

		$.arcticmodal({
			type: 'ajax',
			url: url
		});
	});

	$(document).on('click', '[data-popup-gallery]', function (e) {

		e.preventDefault();

		$.arcticmodal('close');

		var $t = $(this);
		var index = void 0;
		var galleryName = $t.attr('data-popup-gallery');
		var src = $t.attr('href');
		var caption = $t.attr('data-popup-caption');

		var galleryActions = function galleryActions($el, images, currentIndex) {
			var $popup = $el;
			var pics = images;
			var picsCount = images.length;
			var $img = $('img', $popup);
			var $caption = $('.popup__img-caption', $popup);
			var i = currentIndex;

			$popup.on('click', '.popup__btn-prev', function (_) {
				$popup.addClass('popup--loading');
				i == 0 ? i = picsCount - 1 : i--;
				$img.attr('src', pics[i].src).load(function () {
					$popup.removeClass('popup--loading');
					$caption.text(pics[i].caption).css('width', $(this).width());
				});
			});
			$popup.on('click', '.popup__btn-next, img', function (_) {
				$popup.addClass('popup--loading');
				i == picsCount - 1 ? i = 0 : i++;
				$img.attr('src', pics[i].src).load(function () {
					$popup.removeClass('popup--loading');
					$caption.text(pics[i].caption).css('width', $(this).width());
				});
			});
		};

		var images = [];
		$('[data-popup-gallery=' + galleryName + ']').each(function (i) {
			var $t = $(this);
			var url = $t.attr('href');
			var equal = src.localeCompare(url) ? false : true;
			if (equal) {
				index = i;
			}
			images.push({
				'src': url,
				'title': $t.attr('title'),
				'caption': $t.attr('data-popup-caption')
			});
		});

		$('#popupImage').arcticmodal({
			beforeOpen: function beforeOpen(data, el) {
				var $popup = $(el);

				galleryActions($popup, images, index);

				$('.header').css('paddingRight', getScrollbarSize());

				$popup.addClass('popup--loading').closest('.arcticmodal-container').addClass('arcticmodal-img');

				$('img', $popup).attr({
					'src': src,
					'style': 'max-height:' + ($(window).height() - 170) + 'px'
				}).load(function () {
					$popup.removeClass('popup--loading');
					$('.popup__img-caption', $popup).text(caption).css('width', $(this).width());
				});
			}
		});
	});

	spoiler('.b-price__header', '.b-price__body', 200);
	if (viewportWidth < mediaBreakpoint.xl) {
		spoiler('.tab-price__header', '.tab-price__content', 200);
	} else {
		$('.tab-price__content').each(function () {
			var $t = $(this);
			$t.closest('.b-price__body').append($t.detach());
		});
		$(document).on('click', '.tab-price:not(.active)', function () {
			$(this).addClass('active').siblings().removeClass('active').closest('.b-price__body').find('.tab-price__content').removeClass('active').eq($(this).index()).addClass('active');
		});
	}

	spoiler('.r-category__header', '.r-category__list', 200);

	$(document).on('click', '.menu-trigger', function () {
		$('body').toggleClass('nav-showed');
	});

	$(document).on('click', '.js-animated-link', function (e) {
		var _this = this;

		e.preventDefault();
		var scrollBarSize = getScrollbarSize();
		$('html').addClass('not-loaded');
		if (scrollBarSize) {
			$('body').css('paddingRight', scrollBarSize);
			$('.header').css('paddingRight', scrollBarSize);
		}
		setTimeout(function (_) {
			window.location = $(_this).attr('href');
		}, 600);
	});

	// close on focus lost
	$(document).click(function (e) {
		var $trg = $(e.target);
		if (!$trg.closest(".parent-element").length && !$trg.hasClass('trigger-class') || $trg.hasClass('close-btn')) {
			$('.block').removeClass('active');
			$('.spoiler').slideUp(200);
		}
	});
});

// content page
$(function () {
	$('.js-content-images').find('img').each(function () {
		var $t = $(this);
		$t.wrap('<div class="content-img"></div>');
		if ($t.css('float') == 'left' || $t.attr('align') == 'left') {
			$t.parent().addClass('content-img--left');
		} else if ($t.css('float') == 'right' || $t.attr('align') == 'right') {
			$t.parent().addClass('content-img--right');
		}
	});
	$('.content table').each(function () {
		if ($(this).find('th').length) $(this).addClass('styled-table');
	});
	$('.styled-table').each(function () {
		var $t = $(this);
		var $th = $t.find('tr:first').children();
		var rows = $t.find('tr');
		var tHeaders = [];
		for (var i = 0; i < $th.length; i++) {
			tHeaders.push($($th[i]).text());
		}
		for (var _i = 1; _i < rows.length; _i++) {
			var $tds = $(rows[_i]).find('td');
			$tds.each(function (i, item) {
				$(item).attr('data-th', tHeaders[i]);
			});
		}
	});
});

// top slider text fix
$(function () {
	var contentWidth = $('.top-slider__content').width();
	$('.top-slider__item').each(function (i, item) {
		var $slide = $(item);
		var textWidth = $slide.find('.top-slider__text span').width();
		if (textWidth < contentWidth) {
			$('.top-slider__content', $slide).addClass('top-slider__content--full');
		}
	});
});

$(window).on("load", function () {
	$(".dot").dotdotdot({ watch: "window" });

	if ($('#mapit').length && window.google) {
		mapInit();
	}
});

function mapInit() {
	var mapElem = document.getElementById('mapit');
	var inlineData = $(mapElem).data();
	var mapOptions = {
		center: {
			lat: inlineData.lat,
			lng: inlineData.lng
		},
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		styles: [{ "featureType": "all", "elementType": "all", "stylers": [{ "hue": "#ff0000" }, { "saturation": -100 }, { "lightness": -30 }] }, { "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "color": "#ffffff" }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "color": "#353535" }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#656565" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "on" }] }, { "featureType": "poi", "elementType": "geometry.fill", "stylers": [{ "color": "#505050" }] }, { "featureType": "poi", "elementType": "geometry.stroke", "stylers": [{ "color": "#787878" }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#454545" }] }, { "featureType": "transit", "elementType": "labels", "stylers": [{ "saturation": 100 }, { "lightness": -40 }, { "invert_lightness": true }, { "gamma": 1.5 }, { "color": "#e94c3d" }] }, { "featureType": "transit.station", "elementType": "all", "stylers": [{ "visibility": "on" }, { "saturation": "44" }, { "lightness": "-28" }, { "hue": "#ff5c00" }] }, { "featureType": "transit.station", "elementType": "geometry.fill", "stylers": [{ "saturation": "-6" }, { "color": "#c27c7c" }] }, { "featureType": "transit.station", "elementType": "geometry.stroke", "stylers": [{ "saturation": "0" }, { "lightness": "8" }, { "color": "#ae5252" }] }, { "featureType": "transit.station", "elementType": "labels.text.fill", "stylers": [{ "lightness": "5" }, { "color": "#e94c3d" }] }, { "featureType": "transit.station", "elementType": "labels.text.stroke", "stylers": [{ "weight": "2.51" }, { "color": "#ffd5d1" }] }, { "featureType": "transit.station", "elementType": "labels.icon", "stylers": [{ "color": "#555555" }] }]

	};
	var map = new google.maps.Map(mapElem, mapOptions);
	var contentString = '' + '<div class="map-info">' + '<div>' + inlineData.title + '</div>' + '</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(inlineData.lat, inlineData.lng),
		map: map,
		icon: inlineData.marker,
		title: inlineData.title
	});
	marker.addListener('click', function () {
		infowindow.open(map, marker);
	});
}

function getScreenSize() {
	var screenSize = window.getComputedStyle(document.querySelector('body'), ':after').getPropertyValue('content');
	screenSize = parseInt(screenSize.match(/\d+/));
	return screenSize;
}

function getScrollbarSize() {
	var scrollbarSize = undefined;
	if (scrollbarSize === undefined) {
		var scrollDiv = document.createElement("div");
		scrollDiv.style.cssText = 'width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;';
		document.body.appendChild(scrollDiv);
		scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
		document.body.removeChild(scrollDiv);
	}
	return scrollbarSize;
}

function spoiler(spoilerTitle, spoilerBody, time) {
	var activeClass = 'active';
	var $titles = $(spoilerTitle);
	var $bodys = $(spoilerBody);

	$bodys.slideUp(0);
	$titles.first().addClass(activeClass).next().slideToggle(0);
	$(document).on('click', spoilerTitle, function () {
		var $t = $(this);
		if (!$t.hasClass(activeClass) && viewportWidth >= mediaBreakpoint.md) {
			$titles.removeClass(activeClass);
			$bodys.slideUp(time);
			$t.toggleClass(activeClass).next().slideToggle(time);
		} else {
			$t.toggleClass(activeClass).next().slideToggle(time);
		}
	});
}

function SVGRoad(elem) {
	this.pathLength = elem.getTotalLength();
	this.setStrokeDasharrayInPercent = function () {
		var strokeDasharray = "";
		for (var i = 0; i < arguments.length; i++) {
			strokeDasharray += arguments[i] / 100 * this.pathLength + " ";
		}
		elem.style.strokeDasharray = strokeDasharray;
	};

	this.setStrokeDashoffsetInPercent = function (strokeDashoffset) {
		elem.style.strokeDashoffset = strokeDashoffset / 100 * this.pathLength;
	};
}

function objectFitFallback(selector) {
	// if (true) {
	if ('objectFit' in document.documentElement.style === false) {
		for (var i = 0; i < selector.length; i++) {
			var that = selector[i];
			var imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
			var dataFit = that.getAttribute('data-object-fit');
			var fitStyle = void 0;
			if (dataFit === 'cover') {
				fitStyle = 'cover';
			} else {
				fitStyle = 'contain';
			}
			var parent = that.parentElement;
			if (imgUrl) {
				parent.style.backgroundImage = 'url(' + imgUrl + ')';
				parent.classList.add('fit-img');
				parent.classList.add('fit-img--' + fitStyle);
			}
		};
	}
};
//# sourceMappingURL=main.js.map
